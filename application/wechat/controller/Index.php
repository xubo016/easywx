<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 微信开发
// +----------------------------------------------------------------------
namespace app\wechat\controller;

use EasyWeChat\Foundation\Application;
use think\facade\Config;
use think\Controller;
 
class Index extends Controller
{
  // 处理接收回复消息
  public function initialize()
  {
    $accept = new Accept();
    if(!empty(input('get.echostr'))){
      $this->index();
    } else {
      $accept->acceptMsg();
    }
  }
  
  // 公众号连接
  public function index()
  {
    $options = Config::get('wewhat.wxConfig');
    $app = new Application($options);
    $response = $app->server->serve();
    // 将响应输出
    return $response->getContent();
  }

}
?>