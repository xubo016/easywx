<?php
// +----------------------------------------------------------------------
// | astp [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 公众号菜单
// +----------------------------------------------------------------------
namespace app\wechat\controller;

use EasyWeChat\Foundation\Application;
use think\facade\Config;
use think\Controller;
 
class Menulst extends Controller
{
  // 创建自定义菜单
  public function createMenu()
  {
    $options = Config::get('wewhat.wxConfig');
    $app = new Application($options);
    $menu = $app->menu;
    // 删除全部菜单
    $menu->destroy();
    $buttons = [
      [
        "type" => "view",
        "name" => "赤峰资讯",
        "url"  => "https://mp.weixin.qq.com/mp/homepage?__biz=MzU4Njg1MDk5OQ==&hid=3&sn=4804b1ed9d898386c0de7eeabeab833f&scene=1&devicetype=android-28&version=27000634&lang=zh_CN&nettype=WIFI&ascene=7&session_us=gh_1bb19155e98e&wx_header=1"
      ],
      [
        "name"       => "免费发布",
        "sub_button" => [
          [
            "type" => "miniprogram",
            "name" => "同城信息平台",
            "url"  => "http://www.cfckck.com",
            "appid" => "wx286b93c14bbf93aa",
            "pagepath" => "pages/lunar/index",
          ],
          [
            "type" => "view",
            "name" => "图文消息",
            "url"  => "http://nmtguj.natappfree.cc/wewhat/article_img/index",
          ],
          [
            "type" => "view",
            "name" => "视频消息",
            "url"  => "http://nmtguj.natappfree.cc/wewhat/Article_video/index",
          ],
        ],
      ],
      [
        "name"       => "联系我们",
        "sub_button" => [
          [
            "type" => "view",
            "name" => "赤峰汇潮网络",
            "url"  => "https://h.eqxiu.com/s/k4ArlFfw"
          ],
          [
            "type" => "click",
            "name" => "热线电话",
            "key"  => "key001"
          ],
          [
            "type" => "view",
            "name" => "我的活动",
            "url"  => "http://nmtguj.natappfree.cc/wewhat/user_info/index"
          ],
          [
            "type" => "view",
            "name" => "更多活动",
            "url"  => "http://nmtguj.natappfree.cc/wewhat/article_more/index"
          ],
        ],
      ],
    ];
    // 创建自定菜单
    $menu->add($buttons);

    if( $menu ){
      return '设置成功';
    }

  }

}
?>