<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 日志逻辑层
// +----------------------------------------------------------------------
namespace app\admin\service;

class Modular
{
  public static function combined()
  {
    $where['module'] = request()->module();
    $where['controller'] = request()->controller();
    $where['function'] = request()->action();
    $result['url'] = $where;
    $result['string'] = self::string($where);
    return $result;
  }

  private static function string($where)
  {
    //获取除了域名和后缀外的url，是字符串
    $parameter = request()->path() ? request()->path() : null;
    //将字符串转化为数组
    $parameter = explode('/',$parameter);
    //剔除url中的模块、控制器和方法
    foreach ($parameter as $key => $value) {
      if($value != $where['module'] and $value != $where['controller'] and $value != $where['function']) {
        $param[] = $value;
      }
    }
    if(isset($param) and !empty($param)) {
      //确定有参数
      $string = '';
      foreach ($param as $key => $value) {
        //奇数为参数的参数名，偶数为参数的值
        if($key%2 !== 0) {
          //过滤只有一个参数和最后一个参数的情况
          if(count($param) > 2 and $key < count($param)-1) {
            $string.=$value.'&';
          } else {
            $string.=$value;
          }
        } else {
          $string.=$value.'=';
        }
      } 
    } else {
      $string = [];
      $param = request()->param();
      foreach ($param as $key => $value) {
        if(!is_array($value)) {
          //这里过滤掉值为数组的参数
          $string[] = $key.'='.$value;
        }
      }
      $string = implode('&',$string);
    }
    return $string;
  }

} 
?>