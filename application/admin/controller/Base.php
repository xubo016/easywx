<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 基类权限管理
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\service\Modular as ModularService;
use think\Controller;
use think\Db;
use Session;

class Base extends Controller
{
  /**
   * 检查操作用户的ip是否在黑名单里
   * 检查用户是否登录
   * 检查用户访问的url在不在其角色组的权限范围内
   * @return [type] [description]
   */
  protected function initialize()
  {
    $this->blackIP();
    //检查是否登录
    if(!Session::has('admin')) {
      $this->redirect('admin/login/index');
    }
    $result = ModularService::combined();
    $result['url']['type'] = 1;
    //得到用户的权限菜单
    $menus = Db::name('auth_group')->where('id',Session::get('group_id'))->value('permissions');
    //将得到的菜单id集成的字符串拆分成数组
    $menus = explode(',',$menus);
    if(!empty($result['string'])) {
      //检查该带参数的url是否设置了权限
      $param_menu = Db::name('auth_rule')->where($result['url'])->where('parameter',$result['string'])->find();
      if(!empty($param_menu)) {
        //该url的参数设置了权限，检查用户有没有权限
        if(false == in_array($param_menu['id'],$menus)) {
          return $this->error('缺少权限');
        }
      } else {
        //该url带参数状态未设置权限，检查该url去掉参数时，用户有无权限
        $menu = Db::name('auth_rule')->where($result['url'])->find();
        if(!empty($menu)) {
          if(empty($menu['parameter'])) {
            if(!in_array($menu['id'],$menus)) {
              return $this->error('缺少权限');
            }
          }
        }
      }
    } else {
      //用户访问的url里没有参数
      $menu = Db::name('auth_rule')->where($result['url'])->find();
      if(!empty($menu)) {
        if(empty($menu['parameter'])) {
          if(!in_array($menu['id'],$menus)) {
            return $this->error('缺少权限');
          }
        }
      }  
    }
  }

  //检查当前ip是不是在黑名单
  private function blackIP()
  {
    $black_ip = Db::name('web_config')->where('web','web')->value('black_ip');
    if(!empty($black_ip)) {
      //转化成数组
      $black_ip = explode(',',$black_ip);
      //获取当前访问的ip
      $ip = $this->request->ip();
      if(in_array($ip,$black_ip)) {
        //清空session
        if(Session::has('admin')) {
          Session::delete('admin');
        }
        return $this->error('你已被封禁！','admin/login/index');
      }
    }
  }
}
?>