<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员日志操作
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\Log as LogModel;
use think\Db;

class Log extends Base
{
  public function index()
  {
    $model = new LogModel();
    $post = $this->request->param();
    $log = $model->getAll($post);
    $this->assign('log',$log);
    //身份列表
    $auth_group = Db::name('auth_group')->select();
    $this->assign('auth_group',$auth_group);
    $info['menu'] = Db::name('auth_rule')->select();
    $info['admin'] = Db::name('admin')->select();
    $this->assign('info',$info);
    return $this->fetch();
  }
}

?>