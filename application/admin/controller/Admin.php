<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员管理
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\Admin as AdminModel;
use app\model\AuthGroup as AuthGroupModel;
use app\validate\admin\Admin as AdminValidate;
use app\validate\admin\NickName as NickNameValidate;
use app\validate\admin\Password as PasswordValidate;
use think\Db;
use Session;

class Admin extends Base
{
  public function lst()
  {
    $post = $this->request->param();
    $admin = AdminModel::getAdmin($post);
    $this->assign('admin',$admin);

    $info['cates'] = AuthGroupModel::all();
    $this->assign('info',$info);
    return $this->fetch();
  }

  public function addEdit()
  {
    //获取管理员id
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $model = new AdminModel();
    if($id > 0) {
      //是修改操作
      if($this->request->isPost()) {
        (new AdminValidate())->goCheck();
        $post = $this->request->post();
        $result = $model->edit($post,$id);
        if($result) {
          addLog($model->id);//写入日志
          return $this->success('修改管理员信息成功','admin/admin/lst');
        } else {
          return $this->error('修改失败');
        }
      } else {
        $info['admin'] = $model->where('id',$id)->find();
        $info['auth_group'] = Db::name('auth_group')->where('status','=',1)->select();
        $this->assign('info',$info);
        return $this->fetch();
      }
    } else {
      //是新增操作
      if($this->request->isPost()) {
        (new AdminValidate())->goCheck();
        (new PasswordValidate())->goCheck();
        $post = $this->request->post();
        $result = $model->add($post);
        if($result) {
          addLog($model->id);//写入日志
          return $this->success('添加管理员成功','admin/admin/lst');
        } else {
          return $this->error('添加管理员失败');
        }
      } else {
        //非提交操作
        $info['auth_group'] = Db::name('auth_group')->where('status','=',1)->select();
        $this->assign('info',$info);
        return $this->fetch();
      }
    }
  }

  public function delete()
  {
    if($this->request->isAjax()) {
      $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
      if($id == 1) {
        return $this->error('网站所有者不能被删除');
      }
      if($id == Session::get('admin')) {
        return $this->error('自己不能删除自己');
      }
      if(false == Db::name('admin')->where('id',$id)->delete()) {
        return $this->error('删除失败');
      } else {
        addLog($id);//写入日志
        return $this->success('删除成功','admin/admin/lst');
      }
    }
  }

  /**
   * 管理员个人资料修改
   * @return [type] [description]
   */
  public function personal()
  {
    //获取管理员id
    $id = Session::get('admin');
    $model = new AdminModel();
    if($id > 0) {
      //修改操作
      if($this->request->isPost()) {
        (new NickNameValidate())->goCheck();
        //提交操作
        $post = $this->request->post();
        if(false == $model->allowField(true)->save($post,['id'=>$id])) {
          return $this->error('修改失败');
        } else {
          addLog($model->id);//写入日志
          return $this->success('修改个人信息成功','admin/admin/personal');
        }
      } else {
        //非提交操作
        $info['admin'] = $model->where('id',$id)->find();
        $this->assign('info',$info);
        return $this->fetch();
      }
    } else {
      return $this->error('id不正确');
    }
  }

  /**
   * 修改密码
   * @return [type] [description]
   */
  public function editPassword()
  {
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    if($id > 0) {
      if($id == Session::get('admin')) {
        (new PasswordValidate())->goCheck();
        $post = $this->request->post();
        $admin = Db::name('admin')->where('id',$id)->find();
        if(password($post['password_old']) == $admin['password']) {
          if(false == Db::name('admin')->where('id',$id)->update(['password'=>password($post['password'])])) {
            return $this->error('修改失败');
          } else {
            addLog();//写入日志
            return $this->success('修改成功','admin/main/index');
          }
        } else {
          return $this->error('原密码错误');
        }
      } else {
        return $this->error('不能修改别人的密码');
      }
    } else {
      $id = Session::get('admin');
      $this->assign('id',$id);
      return $this->fetch();
    }
  }
}

?>