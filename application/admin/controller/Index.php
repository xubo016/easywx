<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 左边、侧边栏、头部公共文件
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\AuthRule as AuthRuleModel;
use think\Controller;
use Session;
use think\Db;

class Index extends Base
{
  public function index()
  {
    $menus = AuthRuleModel::getAllAdminMenu();
    $this->assign('menus',$menus);
    $cookie = Db::name('admin')->where('id',Session::get('admin'))->find();
    $this->assign('cookie',$cookie);
    return $this->fetch();
  }
}
?>