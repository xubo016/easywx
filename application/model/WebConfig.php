<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 网站设置模型
// +----------------------------------------------------------------------
namespace app\model;

use think\Model;

class WebConfig extends Model
{
  public function edit($post)
  {
    if(empty($post['is_log'])) {
      $post['is_log'] = 0;
    } else {
        $post['is_log'] = $post['is_log'];
    }
    $result = $this->where('web','web')->update($post);
    return $result;
  }
  
}
?>