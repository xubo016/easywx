<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员模型
// +----------------------------------------------------------------------
namespace app\model;

use think\Model;
use Session;

class Attachment extends Model
{
  public function admin()
  {
    //关联管理员表一对一
    return $this->belongsTo('admin','admin_id','id');
  }

  public function getAll($post)
  {
    if (isset($post['keywords']) and !empty($post['keywords'])) {
      $where['filename'] = ['like', '%' . $post['keywords'] . '%'];
    }
    if (isset($post['status']) and ($post['status'] == 1 or $post['status'] === '0' or $post['status'] == -1)) {
      $where['status'] = $post['status'];
    }
    if(isset($post['create_time']) and !empty($post['create_time'])) {
      $min_time = strtotime($post['create_time']);
      $max_time = $min_time + 24 * 60 * 60;
      $where['create_time'] = [['>=',$min_time],['<=',$max_time]];
    }
    $attachment = empty($where) ? $this->order('create_time desc')->paginate(20) : $this->where($where)->order('create_time desc')->paginate(20,false,['query'=>$post]);
    return $attachment;
  }

  public function add($module,$use,$info)
  {
    $data = [];
    $data['module'] = $module;
    $data['filename'] = $info->getFilename();//文件名
    $data['filepath'] = '/'.'uploads' . '/' . $module . '/' . $use . '/' . $info->getSaveName();//文件路径
    $data['fileext'] = $info->getExtension();//文件后缀
    $data['filesize'] = $info->getSize();//文件大小
    $data['create_time'] = time();//时间
    $data['uploadip'] = request()->ip();//IP
    $data['user_id'] = Session::has('admin') ? Session::get('admin') : 0;
    if($data['module'] = 'admin') {
      //通过后台上传的文件直接审核通过
      $data['status'] = 1;
      $data['admin_id'] = $data['user_id'];
      $data['audit_time'] = time();
    }
    $data['use'] = request()->has('use') ? request()->param('use') : $use;//用处
    $result = $this->insertGetId($data);
    return $result;
  }

  public function audit($post,$id)
  {
    $admin_id = Session::get('admin');
    $status = $post['status'];
    $result = $this->where('id',$id)->update(['status'=>$status,'admin_id'=>$admin_id,'audit_time'=>time()]);
    return $result;
  }
}
?>