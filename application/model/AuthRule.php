<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 系统模型
// +----------------------------------------------------------------------
namespace app\model;

use app\admin\service\AuthRule as AuthRuleService;
use think\Model;
use think\facade\Session;

class AuthRule extends Model
{
  public static function getAllAdminMenu()
  {
    $menu = self::where(['is_display'=>1])->order('sort asc')->select();
    //删除不在当前角色权限里的菜单，实现隐藏
    $menus = AuthRuleService::getAuth($menu);
    return $menus;
  }

  public static function menuList()
  {
    $menu = self::order('sort asc')->select();
    $menus = AuthRuleService::menuTree($menu);
    return $menus;
  }

  public function add($post)
  {
    $result = $this->allowField(true)->save($post);
    return $result;
  }

  public function edit($post,$id)
  {
    //如果关闭默认展开，给默认值0
    if(empty($post['is_open'])) {
      $post['is_open'] = 0;
    }
    $result = $this->allowField(true)->save($post,['id'=>$id]);
    return $result;
  }

  public function sort($post)
  {
    $i = 0;
    foreach ($post['id'] as $k => $val) {
      $sort = self::where('id',$val)->value('sort');
      if($sort != $post['sort'][$k]) {
        $result = self::where('id',$val)->update(['sort'=>$post['sort'][$k]]);
        if($result) {
          $i++;
        } else {
          return false;
        }
      }
    }
    return $i;
  }
}
?>