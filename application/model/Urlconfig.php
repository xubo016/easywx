<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain Url美化模型
// +----------------------------------------------------------------------
namespace app\model;

use think\Model;

class Urlconfig extends Model
{
  public function add($post)
  {
    $result = $this->allowField(true)->save($post);
    return $result;
  }

  public function edit($post,$id)
  { 
    $result = $this->allowField(true)->save($post,['id'=>$id]);
    return $result;
  }
}
?>