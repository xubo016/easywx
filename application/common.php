<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------
use vendor\phpmailer\src\PHPMailer;

// 应用公共文件

/**
 * 管理员密码加密方式
 * @param $password  密码
 * @param $password_code 密码额外加密字符
 * @return string
 */
function password($password)
{
  $password_code = config('admin.password_code');
  return md5($password.$password_code);
}

/**
 * 根据附件表的id返回url地址
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
function getUrl($id)
{
	if ($id) {
		$getUrl = \think\Db::name("attachment")->where(['id' => $id])->find();
		if($getUrl['status'] == 1) {
			//审核通过
			return $getUrl['filepath'];
		} elseif($getUrl['status'] == 0) {
			//待审核
			return '/uploads/xitong/default1.jpg';
		} else {
			//不通过
			return '/uploads/xitong/default2.jpg';
		}
  }
  return false;
}

/**
 * 管理员操作日志
 * @param  [type] $data [description]
 * @return [type]       [description]
 */
function addLog($operation_id='')
{
  //获取网站配置
  $web_config = \think\Db::name('web_config')->where('web','web')->find();
  if($web_config['is_log'] == 1) {
    $data['operation_id'] = $operation_id;
    $data['admin_id'] = \Session::get('admin');//管理员id
    $request = new \think\Request;
    $data['ip'] = request()->ip();//操作ip
    $data['create_time'] = time();//操作时间
    $result = \app\base\service\Modular::combined();
    $data['rule_id'] = empty(\think\Db::name('auth_rule')
      ->where($result['url'])
      ->where('parameter',$result['string'])
      ->value('id'))
      ? 
      \think\Db::name('auth_rule')
      ->where($result['url'])
      ->value('id') 
      : 
      \think\Db::name('auth_rule')
      ->where($result['url'])
      ->where('parameter',$result['string'])->value('id');
    \think\Db::name('log')->insert($data);
  } else {
    //关闭了日志
    return true;
  }
}

function SendMail($address)
{
  // require_once(Env::get('root_path').'vendor/'.'phpmailer/src/PHPMailer.php');
  $mail = new PHPMailer();
  // 设置PHPMailer使用SMTP服务器发送Email
  $mail->IsSMTP();
  // 设置邮件的字符编码，若不指定，则为'UTF-8'
  $mail->CharSet='UTF-8';
  // 添加收件人地址，可以多次使用来添加多个收件人
  $mail->AddAddress($address);
  $data = \think\Db::name('emailconfig')->where('email','email')->find();
  $title = $data['title'];
  $message = $data['content'];
  $from = $data['from_email'];
  $fromname = $data['from_name'];
  $smtp = $data['smtp'];
  $username = $data['username'];
  $password = $data['password'];
  // 设置邮件正文
  $mail->Body=$message;
  // 设置邮件头的From字段。
  $mail->From=$from;
  // 设置发件人名字
  $mail->FromName=$fromname;
  // 设置邮件标题
  $mail->Subject=$title;
  // 设置SMTP服务器。
  $mail->Host=$smtp;
  // 设置为"需要验证" ThinkPHP 的config方法读取配置文件
  $mail->SMTPAuth=true;
  //设置html发送格式
  $mail->isHTML(true);
  // 设置用户名和密码。
  $mail->Username=$username;
  $mail->Password=$password;
  // 发送邮件。
  return($mail->Send());
}