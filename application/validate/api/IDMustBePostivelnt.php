<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 正整数验证
// +----------------------------------------------------------------------
namespace app\validate;

class IDMustBePostivelnt extends ApiValidate
{
  protected $rule =[
    'id' => 'require|isPositiveInteger'
  ];

  protected $message = [
    'id' => 'id必须是正整数'
  ];

}

?>