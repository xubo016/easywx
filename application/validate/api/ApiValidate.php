<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain API 验证基类
// +----------------------------------------------------------------------
namespace app\api\validate;

use app\validate\BaseValidate;
use app\lib\exception\ParameterException;

class ApiValidate extends BaseValidate
{
  //获取http传入的参数、对参数做检验
  public function goCheck()
  {
    $params = request()->param();
    $result = $this->batch()->check($params);
    if (!$result)
    {
      $e = new ParameterException([
        'msg' => $this->error
      ]);
      throw $e;
    }
    else
    {
      return true;
    }
  }

}
?>