<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 验证基类
// +----------------------------------------------------------------------
namespace app\validate;

use think\Validate;
use Exception;

class BaseValidate extends Validate
{
  //正整数验证
  protected function isPositiveInteger($value,$rule='',$data='',$field='')
  {
    if (is_numeric($value) && is_int($value + 0) && ($value + 0) >0){
      return true;
    }
    else
    {
      return false;
    }
  }

  //空值验证
  protected function isNotEmpty($value,$rule='',$data='',$field='')
  {
    if (empty($value)) {
      return false;
    } else {
      return true;
    }
  }

  //手机号验证
  protected function isMobile($value)
  {
    $rule = '^1(3|4|5|6|7|8|9)[0-9]\d{8}$^';
    $result = preg_match($rule,$value);
    if ($result) {
      return true;
    }else{
      return false;
    }
  }

  //获取http传入的数据
  public function getDataByRule($arrays)
  {
    if(array_key_exists('user_id',$arrays) | array_key_exists('uid',$arrays))
    {
      //不允许包含user_id或者uid，防止恶意覆盖user_id外键
      throw new ParameterException([
        'msg' => '参数中含有非法的参数名user_id或者uid'
      ]);
    }
    $newArray = [];
    foreach ($this->rule as $key => $value)
    {
      $newArray[$key] = $arrays[$key];
    }
    return $newArray;
  }

}
?>