<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 网站配置验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class WebConfig extends AdminValidate
{
  protected $rule = [
    'name' => 'require',
    'file_type' => 'require',
    'file_type' => 'require',
  ];

  protected $message = [
    'name.require' => '网站名称不能为空',
    'file_type.require' => '上传类型不能为空',
    'file_size.require' => '上传大小不能为空',
  ];
}
?>