<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class NickName extends AdminValidate
{
  protected $rule = [
    'nickname' => 'require|alpha|unique:admin',
  ];

  protected $message = [
    'nickname.require' => '昵称不能为空',
    'nickname.alpha' => '昵称格式只能是字母',
    'nickname.unique' => '该昵称已被注册',
  ];  
}
?>