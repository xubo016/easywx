<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 验证基类
// +----------------------------------------------------------------------
namespace app\validate\admin;

use app\validate\BaseValidate;

class AdminValidate extends BaseValidate
{
  public function goCheck()
  {
    $result = $this->check(request()->param());
    if (!$result)
    {
      $controller = new ControllerValidate();
      $controller->vailErr($this->error);
    }
    else
    {
      return true;
    }
  }

}
?>