<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class Admin extends AdminValidate
{
  protected $rule = [
    'name' => 'require|chs',
    'nickname' => 'require|alpha|unique:admin',
    'group_id' => 'require|isNotEmpty',
    'tel' => 'require|unique:admin|isMobile'
  ];

  protected $message = [
    'name.require' => '用户名不能为空',
    'name.chs' => '用户名格式只能是汉子',
    'nickname.require' => '昵称不能为空',
    'nickname.alpha' => '昵称格式只能是字母',
    'nickname.unique' => '该昵称已被注册',
    'group_id.require' => '请选择管理员分组',
    'group_id.isNotEmpty' => '请选择管理员分组',
    'tel.require' => '手机号不能为空',
    'tel.unique' => '手机号已被注册',
    'tel.isMobile' => '手机号格式不正确',
  ];

  public function scenePersonal()
  {
    return $this->remove(['name','group_id']);
  }   
}
?>