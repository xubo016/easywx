/*
SQLyog v10.2 
MySQL - 5.7.26 : Database - easywx
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`easywx` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `easywx`;

/*Table structure for table `ew_admin` */

DROP TABLE IF EXISTS `ew_admin`;

CREATE TABLE `ew_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `name` varchar(100) NOT NULL COMMENT '真实姓名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `tel` varchar(11) DEFAULT NULL COMMENT '电话',
  `thumb` int(11) NOT NULL DEFAULT '1' COMMENT '管理员头像',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `login_time` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最后登录ip',
  `group_id` int(2) NOT NULL DEFAULT '1' COMMENT '管理员分组',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `admin_cate_id` (`group_id`) USING BTREE,
  KEY `nickname` (`nickname`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `ew_admin` */

insert  into `ew_admin`(`id`,`nickname`,`name`,`password`,`tel`,`thumb`,`create_time`,`update_time`,`login_time`,`login_ip`,`group_id`) values (1,'astp','astp','ce1e6cf04af64102bfb4eedee02f28d3','1',8,1510885948,1553442063,1565773778,'127.0.0.1',1),(4,'admin','许波','53206684e349eeb287fade2c764eb191','15124954484',9,0,0,NULL,NULL,1);

/*Table structure for table `ew_attachment` */

DROP TABLE IF EXISTS `ew_attachment`;

CREATE TABLE `ew_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` char(15) NOT NULL DEFAULT '' COMMENT '所属模块',
  `filename` char(50) NOT NULL DEFAULT '' COMMENT '文件名',
  `filepath` char(200) NOT NULL DEFAULT '' COMMENT '文件路径+文件名',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `fileext` char(10) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `uploadip` char(15) NOT NULL DEFAULT '' COMMENT '上传IP',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未审核1已审核-1不通过',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL COMMENT '审核者id',
  `audit_time` int(11) NOT NULL COMMENT '审核时间',
  `use` varchar(200) DEFAULT NULL COMMENT '用处',
  `download` int(11) NOT NULL DEFAULT '0' COMMENT '下载量',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `filename` (`filename`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='附件表';

/*Data for the table `ew_attachment` */

insert  into `ew_attachment`(`id`,`module`,`filename`,`filepath`,`filesize`,`fileext`,`user_id`,`uploadip`,`status`,`create_time`,`admin_id`,`audit_time`,`use`,`download`) values (8,'admin','eb3a3b14edcd80d5e6e5ffbbc5ccc3c6.png','/uploads/admin/admin_thumb/20190722\\eb3a3b14edcd80d5e6e5ffbbc5ccc3c6.png',789182,'png',1,'127.0.0.1',1,1563810375,1,1563810375,'admin_thumb',0),(9,'admin','d9f2ff9e026ee40206104953a00aa568.png','/uploads/admin/admin_thumb/20190722\\d9f2ff9e026ee40206104953a00aa568.png',789182,'png',1,'127.0.0.1',1,1563810457,1,1563810457,'admin_thumb',0);

/*Table structure for table `ew_auth_group` */

DROP TABLE IF EXISTS `ew_auth_group`;

CREATE TABLE `ew_auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `permissions` text COMMENT '权限菜单',
  `create_time` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1默认开启2默认关闭',
  `desc` text COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `ew_auth_group` */

insert  into `ew_auth_group`(`id`,`name`,`permissions`,`create_time`,`status`,`desc`) values (1,'超级管理员','2,1,11,12,3,8,9,10,4,5,6,7,17,18,13,14,20,19,21,32,22,23,24,25,26,27,28,29,30,31,33,40,34,35,36,37,38,39',0,1,'超级管理员，拥有最高权限！');

/*Table structure for table `ew_auth_rule` */

DROP TABLE IF EXISTS `ew_auth_rule`;

CREATE TABLE `ew_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL DEFAULT '' COMMENT '名称',
  `module` char(50) DEFAULT NULL COMMENT '模块',
  `controller` char(100) DEFAULT NULL COMMENT '控制器',
  `function` char(100) DEFAULT NULL COMMENT '方法',
  `parameter` char(50) DEFAULT NULL COMMENT '参数',
  `desc` varchar(250) DEFAULT NULL COMMENT '描述',
  `is_display` int(1) NOT NULL DEFAULT '1' COMMENT '1显示在左侧菜单2只作为操作节点',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1权限节点2普通节点',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态1为启用',
  `condition` char(100) DEFAULT '',
  `pid` int(5) NOT NULL DEFAULT '0' COMMENT '父栏目0为顶级菜单',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `is_open` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0默认闭合1默认展开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='权限节点';

/*Data for the table `ew_auth_rule` */

insert  into `ew_auth_rule`(`id`,`name`,`module`,`controller`,`function`,`parameter`,`desc`,`is_display`,`type`,`icon`,`status`,`condition`,`pid`,`create_time`,`sort`,`is_open`) values (1,'管理员','admin','admin','lst','','管理员列表                                                  ',1,1,'fa-user',1,'',2,NULL,0,0),(32,'短信设置','admin','SmsConfig','index','','短信配置首页。           ',1,1,'fa-comments',1,'',21,1554731788,0,0),(2,'系统菜单','','','','','            系统菜单              ',1,1,'fa-cog',1,'',0,NULL,0,0),(3,'权限组','admin','auth_group','lst','','            用户组列表          ',1,1,'fa-dot-circle-o',1,'',2,NULL,0,0),(4,'系统菜单','admin','auth_rule','lst','','            系统列表                    ',1,1,'fa-share-alt',1,'',2,NULL,0,0),(5,'删除系统菜单','admin','AuthRule','delete','','删除系统菜单',2,1,'',1,'',4,NULL,0,0),(6,'添加系统菜单','admin','AuthRule','addEdit','','添加修改系统菜单',2,1,'',1,'',4,NULL,0,0),(7,'系统菜单排序','admin','base','sort','','系统菜单排序',2,1,'',1,'',4,NULL,0,0),(8,'添加权限组','admin','AuthGroup','addEdit','','            权限组新增/修改            ',2,1,'',1,'',3,1552967170,0,0),(9,'删除权限组','admin','AuthGroup','delete','','删除权限组',2,1,'',1,'',3,1552967269,0,0),(10,'权限组预览','admin','AuthGroup','preview','','权限组预览',2,1,'',1,'',3,1552967327,0,0),(11,'新增管理员','admin','admin','addEdit','','新增修改管理员                  ',2,1,'',1,'',1,1553044733,0,0),(12,'删除管理员','admin','admin','delete','','删除管理员',2,1,'',1,'',1,1553044826,0,0),(13,'个人','','','','','个人信息修改                           ',1,1,'fa-user',1,'',0,1553045051,0,0),(14,'个人信息','admin','admin','personal','','个人信息修改    ',1,1,'fa-user',1,'',13,1553045803,0,0),(15,'管理员登录','admin','login','index','','管理员登录',2,2,'',1,'',0,1553091902,1,0),(16,'管理员退出','admin','login','logout','','管理员退出                       ',2,2,'',1,'',0,1553091936,1,0),(17,'网站设置','admin','webConfig','index','','网站设置   ',1,1,'fa-bullseye',1,'',2,1553093381,0,0),(18,'修改网站设置','admin','webConfig','edit','','            修改网站设置                  ',2,1,'',1,'',17,1553093739,0,0),(19,'图片上传','admin','common','upload','','图片上传                       ',2,1,'',1,'',0,1553183549,1,0),(20,'修改密码','admin','admin','editpassword','','管理员修改个人密码。',1,1,'fa-unlock-alt',1,'',13,1553184676,0,0),(21,'内容','','','','','内容管理   ',1,1,'fa-th-large',1,'',0,1553256973,0,0),(22,'附件','admin','attachment','lst','','            附件列表           ',1,1,'fa-cube',1,'',21,1553257092,0,0),(23,' 附件审核','admin','attachment','audit','','附件审核。         ',2,1,'',1,'',22,1553257199,0,0),(24,'附件上传','admin','attachment','upload','','附件上传。',2,1,'',1,'',22,1553257246,0,0),(25,'附件下载','admin','attachment','download','','附件下载       ',2,1,'',1,'',22,1553257295,0,0),(26,'附件删除','admin','attachment','delete','','附件删除',2,1,'',1,'',22,1553257334,0,0),(27,'操作日志','admin','log','index','','管理员操作日志     ',1,1,'fa-pencil',1,'',21,1553273460,0,0),(28,'URL设置','admin','urlconfig','lst','','            URL设置列表             ',1,1,'fa-code-fork',1,'',21,1553341204,0,0),(29,'新增/修改url设置','admin','urlconfig','addEdit','','            新增/修改url设置。              ',2,1,'',1,'',28,1553341264,0,0),(30,'启用/禁用url美化','admin','urlconfig','status','','            启用/禁用url美化                      ',2,1,'',1,'',28,1553341305,0,0),(31,'删除url美化规则','admin','urlconfig','delete','','            删除url美化规则。          ',2,1,'',1,'',28,1553341354,0,0),(33,'商城管理','','','','','',1,1,'fa-shopping-bag',1,'',0,NULL,0,0),(34,'轮播图','admin','banner','lst','','',1,1,'',1,'',40,NULL,0,0),(35,'新增轮播图','admin','banner','addEdit','','新增/修改轮播图',2,1,'',1,'',34,NULL,0,0),(36,'删除轮播图','admin','banner','del','','删除轮播图',2,1,'',1,'',34,NULL,0,0),(37,'轮播项','admin','bannerItem','lst','','轮播项',1,1,'',1,'',40,1563874278,0,0),(38,'新增轮播','admin','bannerItem','addEdit','','新增/修改轮播',2,1,'',1,'',37,1563874371,0,0),(39,'删除轮播项','admin','bannerItem','del','','删除轮播项',2,1,'',1,'',37,1563874424,0,0),(40,'轮播','','','','','',1,1,'',1,'',33,1563874593,0,0);

/*Table structure for table `ew_log` */

DROP TABLE IF EXISTS `ew_log`;

CREATE TABLE `ew_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL COMMENT '操作菜单id',
  `admin_id` int(11) DEFAULT NULL COMMENT '操作者id',
  `ip` varchar(100) DEFAULT NULL COMMENT '操作ip',
  `operation_id` varchar(200) DEFAULT NULL COMMENT '操作关联id',
  `create_time` int(11) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `admin_id` (`admin_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ew_log` */

insert  into `ew_log`(`id`,`rule_id`,`admin_id`,`ip`,`operation_id`,`create_time`) values (1,6,1,'127.0.0.1','1',1553436083),(2,14,1,'127.0.0.1','1',1553442033);

/*Table structure for table `ew_smsconfig` */

DROP TABLE IF EXISTS `ew_smsconfig`;

CREATE TABLE `ew_smsconfig` (
  `sms` varchar(10) NOT NULL DEFAULT 'sms' COMMENT '标识',
  `appkey` varchar(200) NOT NULL,
  `secretkey` varchar(200) NOT NULL,
  `type` varchar(100) DEFAULT 'normal' COMMENT '短信类型',
  `name` varchar(100) NOT NULL COMMENT '短信签名',
  `code` varchar(100) NOT NULL COMMENT '短信模板ID',
  `content` text NOT NULL COMMENT '短信默认模板',
  KEY `sms` (`sms`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `ew_smsconfig` */

insert  into `ew_smsconfig`(`sms`,`appkey`,`secretkey`,`type`,`name`,`code`,`content`) values ('sms','12','12','normal','赤峰天一建材装饰店','SMS_172430066','1234');

/*Table structure for table `ew_urlconfig` */

DROP TABLE IF EXISTS `ew_urlconfig`;

CREATE TABLE `ew_urlconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aliases` varchar(200) NOT NULL COMMENT '想要设置的别名',
  `url` varchar(200) NOT NULL COMMENT '原url结构',
  `desc` text COMMENT '备注',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0禁用1使用',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ew_urlconfig` */

insert  into `ew_urlconfig`(`id`,`aliases`,`url`,`desc`,`status`,`create_time`) values (1,'login','base/login/index','后台登录地址。',0,1517621629);

/*Table structure for table `ew_web_config` */

DROP TABLE IF EXISTS `ew_web_config`;

CREATE TABLE `ew_web_config` (
  `web` varchar(20) NOT NULL COMMENT '网站配置标识',
  `name` varchar(200) NOT NULL COMMENT '网站名称',
  `keywords` text COMMENT '关键词',
  `desc` text COMMENT '描述',
  `is_log` int(1) NOT NULL DEFAULT '1' COMMENT '1开启日志0关闭',
  `file_type` varchar(200) DEFAULT NULL COMMENT '允许上传的类型',
  `file_size` bigint(20) DEFAULT NULL COMMENT '允许上传的最大值',
  `statistics` text COMMENT '统计代码',
  `black_ip` text COMMENT 'ip黑名单',
  `url_suffix` varchar(20) DEFAULT NULL COMMENT 'url伪静态后缀',
  KEY `web` (`web`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `ew_web_config` */

insert  into `ew_web_config`(`web`,`name`,`keywords`,`desc`,`is_log`,`file_type`,`file_size`,`statistics`,`black_ip`,`url_suffix`) values ('web','astp后台管理框架','astp,后台管理,thinkphp5,layui','astp是一款基于ThinkPHP5.0.12 + layui2.2.45 + ECharts + Mysql开发的后台管理框架，集成了一般应用所必须的基础性功能，为开发者节省大量的时间。',0,'jpg,png,gif,mp4,zip,jpeg',1000,'','',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
